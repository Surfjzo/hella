/**
 * Hella Backend API
 * routes/company/companyRoutes.js
 * Jean Oliveira 16/12/2017
 *
 * Company routes
 *
 */
'use strict'
let express = require('express'),
	router = express.Router(),
	HellaSchemas = require('../../utils/HellaSchemas'),
	Logger = require('../../utils/Logger'),
	Boom = require('boom'),
	System = require('../../models/classes/System'),
	Joi = require('joi'),
	dbUser = require('../../models/dbCrud/dbUser'),
	dbCompany = require('../../models/dbCrud/dbCompany'),
	Error = require('../../utils/Errors')

const TAG = 'CompanyRoutes'

const newCompanySchema = HellaSchemas.ROUTE_SCHEMAS.NEW_COMPANY_SCHEMA
const updateCompanySchema = HellaSchemas.ROUTE_SCHEMAS.UPDATE_COMPANY_SCHEMA

/* GET existing companies with action name. */
router.get('/:action', async (req, res, next) => {
	try {
		if (req.params && req.params.action) {
			Logger.info(TAG, 'GET User with action ' + req.params.action)
			switch (req.params.action) {
				case 'list':
					if (req.user.id) {
						return res.json(await dbUser.getCompanies(req.user.id))
					} else {
						throw Error.OPERATIONS.INVALID_QUERY
					}
					break
				case 'read':
					if (req.query.companyId) {
						return res.json(await dbCompany.findById(req.query.companyId))
					} else {
						throw Error.OPERATIONS.INVALID_QUERY
					}
					break
				default:
					throw Error.OPERATIONS.INVALID_ACTION
			}
		} else {
			throw Error.OPERATIONS.INVALID_ACTION
		}
	} catch (blockError) {
		if (blockError.status && blockError.content) {
			res.status(blockError.status).json(blockError.content)
		} else {
			res.status(501).json(Boom.notImplemented('I dont know what to do with this', blockError))
		}
	}
})

/* POST user creation and updating. */
router.post('/:action', async (req, res, next) => {
	try {
		if (req.params && req.params.action && req.body.company) {
			Logger.info(TAG, 'POST Updating user'+ req.user.id +'with: ' + req.params.action)
			switch (req.params.action) {
				case 'create':
					const result = await Joi.validate(req.body.company, newCompanySchema)
					if (!result.error) {
						res.status(200).json(await dbCompany.addCompany(req.body.company, req.user.cpf))
					} else {
						throw result.error
					}
					break
				case 'update':
					const result = await Joi.validate(req.body.company, updateCompanySchema)
					if (!result.error) {
						res.status(200).json(await dbCompany.updateCompany(req.body.company.id, req.body.company))
					} else {
						throw result.error
					}
					break
			}
		} else {
			throw Error.OPERATIONS.INVALID_QUERY
		}
	} catch (blockError) {
		if (blockError.status && blockError.content) {
			res.status(blockError.status).json(blockError.content)
		} else {
			res.status(501).json(Boom.notImplemented('I dont know what to do with this', blockError))
		}
	}
})

module.exports = router