/**
 * Hella Backend API
 * routes/system/admin.js
 * Jean Oliveira 09/11/2017
 *
 * Admin route
 *
 */
'use strict'
let express = require('express'),
	router = express.Router(),
	Constants = require('../../utils/constants'),
	Logger = require('../../utils/Logger'),
	Boom = require('boom'),
	System = require('../../models/classes/System'),
	Joi = require('joi')

const TAG = 'AdminRoute'

const postSchema = Joi.object().keys({
	content: Joi.object().required()
}).required()

/* GET existing data with parameter name. */
router.get('/:propertyName', async (req, res, next) => {
	if (req.query.propertyName) {
		Logger.info(TAG, 'GET ' + req.query.propertyName)
		try {
			res.status(200).json(await System.getContentForKey(req.query.propertyName))
		} catch (getError) {
			Logger.error(TAG, getError.toString())
			res.status(501).json(Boom.notImplemented('I dont know what to do with this', getError))
		}
	} else {
		res.status(400).json(Boom.badRequest('You must provide a valid property name, mate!'))
	}
})

/* POST data for a parameter name. */
router.post('/:propertyName', async (req, res, next) => {
	if (req.query.propertyName &&
		Constants.ALLOWED_KEYS.includes(req.query.propertyName) &&
		req.body.content) {
		Logger.info(TAG, 'POST '+ req.query.propertyName)
		try {
			const result = await Joi.validate(req.body, postSchema)
			if (!result.error) {
				res.status(200).json(await System.setContentForKey(req.query.propertyName, req.body.content))
			} else {
				res.status(400).json(Boom.badRequest('You must provide a valid object on the request BODY'))
			}
		} catch (getError) {
			Logger.error(TAG, getError.toString())
			res.status(501).json(Boom.notImplemented('I dont know what to do with this', getError))
		}
	} else {
		Logger.error(TAG, Error.PROPERTY.WRONG_MISSING)
		res.status(400).json(Boom.badRequest('You must provide a valid property name, mate!', Error.PROPERTY.WRONG_MISSING))
	}
})

module.exports = router