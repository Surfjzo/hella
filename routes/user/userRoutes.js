/**
 * Hella Backend API
 * routes/user/userRoutes.js
 * Jean Oliveira 09/11/2017
 *
 * User routes
 *
 */
'use strict'
let express = require('express'),
	router = express.Router(),
	HellaSchemas = require('../../utils/HellaSchemas'),
	Logger = require('../../utils/Logger'),
	Boom = require('boom'),
	System = require('../../models/classes/System'),
	Joi = require('joi'),
	dbUser = require('../../models/dbCrud/dbUser'),
	Error = require('../../utils/Errors')

const TAG = 'UserRoutes'

const updateUserSchema = HellaSchemas.ROUTE_SCHEMAS.UPDATE_USER_SCHEMA

/* GET existing users with action name. */
router.get('/:action', async (req, res, next) => {
	try {
		if (req.params && req.params.action) {
			Logger.info(TAG, 'GET User with action ' + req.params.action)
			switch (req.params.action) {
				case 'read':
					if (req.user.id) {
						return res.json(await dbUser.findById(req.user.id))
					} else {
						throw Error.OPERATIONS.INVALID_QUERY
					}
					break
				default:
					throw Error.OPERATIONS.INVALID_ACTION
			}
		} else {
			throw Error.OPERATIONS.INVALID_ACTION
		}
	} catch (blockError) {
		if (blockError.status && blockError.content) {
			res.status(blockError.status).json(blockError.content)
		} else {
			res.status(501).json(Boom.notImplemented('I dont know what to do with this', blockError))
		}
	}
})

/* POST user creation and updating. */
router.post('/:action', async (req, res, next) => {
	try {
		if (req.params.action && req.params.action === 'update') {
			Logger.info(TAG, 'POST Updating user'+ req.user.id +'with: ' + req.params.action)
			const result = await Joi.validate(req.body.user, updateUserSchema)
			if (!result.error) {
				res.status(200).json(await dbUser.updateUser(req.user.id, req.body.user))
			} else {
				throw result.error
			}
		} else {
			throw Error.OPERATIONS.INVALID_QUERY
		}
	} catch (blockError) {
		if (blockError.status && blockError.content) {
			res.status(blockError.status).json(blockError.content)
		} else {
			res.status(501).json(Boom.notImplemented('I dont know what to do with this', blockError))
		}
	}
})

module.exports = router