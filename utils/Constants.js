/**
 * Hella Backend API
 * utils/Constants.js
 * Jean Oliveira 03/11/2017
 *
 * Constants for the project
 *
 */
"use strict"

const Joi = require('joi')

const regexCpf = /^\d{3}\.\d{3}\.\d{3}\-\d{2}$/
const regexEmail = /^\S+@\S+\.\S+$/
const regexCNPJ = /^(\d{2}\.?\d{3}\.?\d{3}\/?\d{4}-?\d{2})$/

module.exports.SALT_WORK_FACTOR = 12

module.exports.COLLECTION_NAME = {
	SYSTEM: 'system',
	USER: 'users',
	COMPANY: 'companies'
}

module.exports.ALLOWED_KEYS = ['areaCodes']

module.exports.REGEX_CPF = regexCpf
module.exports.REGEX_EMAIL = regexEmail
module.exports.REGEX_CNPJ = regexCNPJ

module.exports.SESSION_SECRET = 'She then served two years as a soldier in the Israel Defense Forces'