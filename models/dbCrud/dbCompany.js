/**
 * Hella Backend API
 * models/deCrud/dbCompany.js
 * Jean Oliveira 03/11/2017
 *
 * DbCompany CRUD
 *
 */
"use strict"
let mongoose = require('mongoose'),
	Successes = require('../../utils/Successes'),
	Errors = require('../../utils/Errors'),
	Enums = require('../../utils/Enums'),
	User = require('../classes/User'),
	Company = require('../classes/Company'),
	Logger = require('../../utils/Logger'),
	Hash = require('../../utils/Hashing'),
	jwt = require('jsonwebtoken'),
	Constants = require('../../utils/Constants')

const TAG = 'DbCompany'

module.exports.findById = async (companyId) => {
	try {
		return await Company.findOne({_id: mongoose.Types.ObjectId(companyId)})
	} catch (findError) {
		Logger.info(TAG, findError.toString())
		throw findError
	}
}

module.exports.addCompany = async (newCompanyData, ownerUserCpf) => {
	try {
		//let newUser = createNewUser(newUserData)
		let newCompany = new Company(newCompanyData)
		if (ownerUserCpf) {
			newCompany.owners.add(ownerUserCpf)
		}
		return await newCompany.save()
	} catch (saveError) {
		Logger.info(TAG, saveError.toString())
		throw saveError
	}
}

module.exports.updateCompany = async (targetCompanyId, updateData) => {
	try {
		return await Company.findOneAndUpdate(
			{ _id: mongoose.Types.ObjectId(targetUserId) },
			{ $set: updateData },
			{ new: true } // Returns the updated object
		).exec()
	} catch (updateError) {
		Logger.info(TAG, updateError.toString())
		throw updateError
	}
}

module.exports.getAll = async () => {
	try {
		return await Company.find().exec()
	} catch (getAllError) {
		Logger.info(TAG, getAllError.toString())
		throw getAllError
	}
}

module.exports.listForOwner = async (userCpf) => {
	try {
		return await Company.find({
			owners: userCpf
		})
	} catch (listCompaniesError) {
		Logger.info(TAG, listCompaniesError.toString())
		throw listCompaniesError
	}
}

module.exports.listForManager = async (userCpf) => {
	try {
		return await Company.find({
			managers: userCpf
		})
	} catch (listCompaniesError) {
		Logger.info(TAG, listCompaniesError.toString())
		throw listCompaniesError
	}
}

module.exports.listForEmployees = async (userCpf) => {
	try {
		return await Company.find({
			employees: userCpf
		})
	} catch (listCompaniesError) {
		Logger.info(TAG, listCompaniesError.toString())
		throw listCompaniesError
	}
}

module.exports.login = async (withUserData) => {
	return new Promise(async (resolve, reject) => {
		try {
			let options = {}
			if (withUserData.email) {
				options.email = withUserData.email
			} else if (withUserData.cpf) {
				options.cpf = withUserData.cpf
			} else {
				return reject(Errors.OPERATIONS.LOGIN)
			}
			let result = await User.findOne(options).exec()
			if (result && result.password) {
				if (await Hash.verifyPassword(withUserData.password, result.password)) {
					let token = await jwt.sign({
						email: result.email,
						cpf: result.cpf,
						name: result.name,
						userTypes: result.types,
						id: result._id,
					}, Constants.SESSION_SECRET)
					let response = {}
					response.token = 'JWT ' + token
					response.result = Successes.OPERATIONS.LOGIN
					return resolve(response)
				} else {
					Logger.info(TAG, 'Unsuccessful login attempt')
					let response = {}
					response.status = Errors.OPERATIONS.LOGIN
					return reject(response)
				}
			} else {
				Logger.info(TAG, 'Unsuccessful login attempt')
				let response = {}
				response.status = Errors.OPERATIONS.LOGIN
				return reject(response)
			}
		} catch (loginError) {
			return reject(loginError)
		}
	})
}