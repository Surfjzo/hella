/**
 * Hella Backend API
 * models/PaymentMethod.js
 * Jean Oliveira 03/11/2017
 *
 * Phone model
 *
 */
"use strict"
let mongoose = require('mongoose')
let Schema = mongoose.Schema
let Enums = require('../../utils/Enums')

const PaymentMethodSchema = Schema({
	paymentType: { type: Enums.PAYMENT_TYPE, required: true },
	percent: { type: Number, min: 1, max: 100, required: true, default: 100 },
	value: { type: Number, required: true },
	paymentId: { type: Schema.types.ObjectId, required: true }
})

let PaymentMethod = mongoose.model('paymentMethod', PaymentMethodSchema)

module.exports = PaymentMethod