/**
 * Hella Backend API
 * models/Company.js
 * Jean Oliveira 03/11/2017
 *
 * Company model
 *
 */
"use strict"
let mongoose = require('mongoose')
let Schema = mongoose.Schema
let Enums = require('../../utils/Enums')
let Address = require('./Address')
let Media = require('./Media')
let Cnae = require('./Cnae')
let Star = require('./Stars')
let CreditCard = require('./CreditCard')

const CompanySchema = Schema({
	name: { type: String, required: true },
	cnpj: { type: String, required: true },
	addresses: { type: [Address], required: true },
	email: { type: String, required: true },
	documents: { type: [Media] },
	cnaes: { type: [Cnae], required: true },
	socialEvalClient: { type: Star.schema, required: true, default: { interactions: 0, points: 0 } },
	socialEvalPro: { type: Star.schema, required: true, default: { interactions: 0, points: 0 } },
	creditCards: { type: [CreditCard] },
	owners: { type: [String], required: true },
	managers: { type: [String], required: true },
	employees: { type: [String], required: true },
	areaCodes: { type: [Enums.AREA_CODE_SCHEMA], required: true },
	creation: { type: Date, required: true, default: Date.now() },
	phones: { type: [String], required: true },
	types: { type: [Enums.USER_TYPE_SCHEMA], required: true, default: [Enums.USER_TYPE.CUSTOMER] },
	able: { type: Boolean, required: true, default: true },
	bankAccount: { type: Enums.BANK_ACCOUNT_SCHEMA }
})

let Company = mongoose.model('company', CompanySchema)

module.exports = Company