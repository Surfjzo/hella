/**
 * Hella Backend API
 * models/User.js
 * Jean Oliveira 03/11/2017
 *
 * User model
 *
 */
"use strict"
let mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	Enums = require('../../utils/Enums'),
	Address = require('./Address'),
	Media = require('./Media'),
	Cnae = require('./Cnae'),
	Star = require('./Stars'),
	CreditCard = require('./CreditCard'),
	Logger = require('../../utils/Logger'),
	Constants = require('../../utils/Constants'),
	Schemas = require('../../utils/HellaSchemas'),
	Hash = require('../../utils/Hashing')

const TAG = 'UserModel'

let cpfMatch = [ Constants.REGEX_CPF, "Informed CPF value doesn\'t match cpf format ({VALUE})" ]
let emailMatch = [ Constants.REGEX_EMAIL, "Informed EMAIL value doesn\'t match email format ({VALUE})" ]

const UserSchema = Schema({
	name: { type: String, trim: true, required: true, minlength: 5 },
	cpf: { type: String, required: true, unique: true, match: cpfMatch },
	email: { type: String, trim: true, required: true, unique: true, match: emailMatch },
	cnpjs: { type: [String], default: [] },
	addresses: { type: [Address.schema], required: true },
	creditCards: { type: [CreditCard.schema] },
	password: { type: String, required: true, minlength: 6 },
	types: { type: [Schemas.DB_SCHEMAS.USER_TYPE_SCHEMA], required: true, default: [Enums.USER_TYPE.CUSTOMER] },
	areaCodes: { type: [Schemas.DB_SCHEMAS.AREA_CODE_SCHEMA], default: [] },
	phones: { type: [Schemas.DB_SCHEMAS.PHONE_SCHEMA], required: true },
	documents: { type: [Media.schema] },
	cnaes: { type: [Cnae.schema] },
	socialEvalClient: { type: Star.schema, required: true, default: { interactions: 0, points: 0 } },
	socialEvalPro: { type: Star.schema, default: { interactions: 0, points: 0 } },
	creation: { type: Date, required: true, default: Date.now() },
	able: { type: Boolean, required: true, default: true },
	bankAccount: { type: Schemas.DB_SCHEMAS.BANK_ACCOUNT_SCHEMA }
})

UserSchema.pre('save', async function (next) {
	if (!this.isModified('password')) {
		return next()
	}

	try {
		this.password = await Hash.passwordHash(this.password)
		next()
	} catch (hashingError) {
		Logger.error(TAG, 'Password hashing error: '+hashingError.toString())
		next(hashingError)
	}
})

let User = mongoose.model('user', UserSchema)

User.prototype.hasType = (typeToCheck) => {
	//TODO implementation of user has type check function
}

User.prototype.isAboveLevel = (typeToCheck) => {
	//TODO implementation of user is above this level
}

module.exports.schema = UserSchema
module.exports = User
